﻿using System.Collections.Generic;
using src.ComponentDetails.FormFactors;
using src.Components.CoolingSystem.Entities;
using src.Components.GPU.Entities;
using src.Components.Motherboards.Entities;
using src.Results;

namespace src.Components.Hull.Entities;

public interface IHull : IComponent, IHullBuilderDirector
{
    public int MaxHeight { get; }
    public int MaxWidth { get; }
    public IReadOnlyList<MotherBoardFormFactor> SupportedFormFactorsMotherBoard { get; }
    public int Length { get; }
    public int Height { get; }
    public int Width { get; }
    public ValidateResultComponent CheckMotherBoard(IMotherBoard motherBoard);
    public ValidateResultComponent CheckCooler(ICoolingSystem coolingSystem);
    public ValidateResultComponent CheckGpu(IGpu? gpu);
}
