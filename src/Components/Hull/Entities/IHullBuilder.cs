﻿using System.Collections.Generic;
using src.ComponentDetails.FormFactors;

namespace src.Components.Hull.Entities;

public interface IHullBuilder
{
    IHullBuilder AddMaxHeight(int maxHeight);
    IHullBuilder AddMaxWidth(int maxWidth);
    IHullBuilder AddSupportedFormFactorsMotherboard(IEnumerable<MotherBoardFormFactor> supportedFormFactorMotherBoard);
    IHullBuilder AddLength(int length);
    IHullBuilder AddWidth(int width);
    IHullBuilder AddHeight(int height);
    IHull Build();
}
