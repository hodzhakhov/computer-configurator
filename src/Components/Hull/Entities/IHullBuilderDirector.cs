﻿namespace src.Components.Hull.Entities;

public interface IHullBuilderDirector
{
    IHullBuilder Direct(IHullBuilder builder);
}
