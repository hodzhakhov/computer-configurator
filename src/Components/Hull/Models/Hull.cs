﻿using System.Collections.Generic;
using System.Linq;
using src.ComponentDetails.FormFactors;
using src.Components.CoolingSystem.Entities;
using src.Components.GPU.Entities;
using src.Components.Hull.Entities;
using src.Components.Motherboards.Entities;
using src.Results;

namespace src.Components.Hull.Models;

public class Hull : IHull
{
    public Hull(
        int maxHeight,
        int maxWidth,
        IEnumerable<MotherBoardFormFactor> supportedFormFactorsMotherBoard,
        int height,
        int width,
        int length)
    {
        MaxHeight = maxHeight;
        MaxWidth = maxWidth;
        SupportedFormFactorsMotherBoard = supportedFormFactorsMotherBoard.ToList();
        Height = height;
        Width = width;
        Length = length;
    }

    public int MaxHeight { get; }
    public int MaxWidth { get; }
    public IReadOnlyList<MotherBoardFormFactor> SupportedFormFactorsMotherBoard { get; }
    public int Length { get; }
    public int Width { get; }
    public int Height { get; }

    public string Name { get; } = "Hull";

    public IHullBuilder Direct(IHullBuilder builder)
    {
        builder.AddMaxHeight(MaxHeight);
        builder.AddMaxWidth(MaxWidth);
        builder.AddSupportedFormFactorsMotherboard(SupportedFormFactorsMotherBoard);
        builder.AddHeight(Height);
        builder.AddLength(Length);
        builder.AddWidth(Width);

        return builder;
    }

    public ValidateResultComponent CheckMotherBoard(IMotherBoard motherBoard)
    {
        if (!SupportedFormFactorsMotherBoard.Contains(motherBoard.FormFactor))
        {
            return new ValidateResultComponent.FailedValidateResultComponent("Motherboard doesn't match hull");
        }

        return new ValidateResultComponent.SuccessValidateResultComponent();
    }

    public ValidateResultComponent CheckCooler(ICoolingSystem coolingSystem)
    {
        if (coolingSystem.Size > MaxHeight)
        {
            return new ValidateResultComponent.FailedValidateResultComponent("Cooling system doesn't fit hull");
        }

        return new ValidateResultComponent.SuccessValidateResultComponent();
    }

    public ValidateResultComponent CheckGpu(IGpu? gpu)
    {
        if (gpu is not null)
        {
            if (gpu.Height > MaxHeight || gpu.Width > MaxWidth)
            {
                return new ValidateResultComponent.FailedValidateResultComponent("GPU doesn't fit hull");
            }
        }

        return new ValidateResultComponent.SuccessValidateResultComponent();
    }
}
