﻿using System;
using System.Collections.Generic;
using System.Linq;
using src.ComponentDetails.FormFactors;
using src.Components.Hull.Entities;

namespace src.Components.Hull.Models;

public class HullBuilder : IHullBuilder
{
    private int _maxHeight;
    private int _maxWidth;
    private IReadOnlyList<MotherBoardFormFactor>? _supportedFormFactorsMotherBoard;
    private int _length;
    private int _height;
    private int _width;

    public IHullBuilder AddMaxHeight(int maxHeight)
    {
        _maxHeight = maxHeight;
        return this;
    }

    public IHullBuilder AddMaxWidth(int maxWidth)
    {
        _maxWidth = maxWidth;
        return this;
    }

    public IHullBuilder AddSupportedFormFactorsMotherboard(IEnumerable<MotherBoardFormFactor> supportedFormFactorMotherBoard)
    {
        _supportedFormFactorsMotherBoard = supportedFormFactorMotherBoard.ToList();
        return this;
    }

    public IHullBuilder AddLength(int length)
    {
        _length = length;
        return this;
    }

    public IHullBuilder AddWidth(int width)
    {
        _width = width;
        return this;
    }

    public IHullBuilder AddHeight(int height)
    {
        _height = height;
        return this;
    }

    public IHull Build()
    {
        return new Hull(
            _maxHeight,
            _maxWidth,
            _supportedFormFactorsMotherBoard ?? throw new ArgumentNullException(nameof(_supportedFormFactorsMotherBoard)),
            _height,
            _width,
            _length);
    }
}
