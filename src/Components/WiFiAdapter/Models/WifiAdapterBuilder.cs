﻿using System;
using src.ComponentDetails.WifiStandardVersions;
using src.Components.WiFiAdapter.Entities;

namespace src.Components.WiFiAdapter.Models;

public class WifiAdapterBuilder : IWifiAdapterBuilder
{
    private WifiStandardVersion? _wifiStandard;
    private bool _bluetoothModule;
    private int _pciVersion;
    private int _powerConsumption;

    public IWifiAdapterBuilder AddWifiStandardVersion(WifiStandardVersion wifiStandard)
    {
        _wifiStandard = wifiStandard;
        return this;
    }

    public IWifiAdapterBuilder AddBluetoothModule(bool bluetoothModule)
    {
        _bluetoothModule = bluetoothModule;
        return this;
    }

    public IWifiAdapterBuilder AddPciVersion(int pciVersion)
    {
        _pciVersion = pciVersion;
        return this;
    }

    public IWifiAdapterBuilder AddPowerConsumption(int powerConsumption)
    {
        _powerConsumption = powerConsumption;
        return this;
    }

    public IWifiAdapter Build()
    {
        return new WifiAdapter(
            _wifiStandard ?? throw new ArgumentNullException(nameof(_wifiStandard)),
            _bluetoothModule,
            _pciVersion,
            _powerConsumption);
    }
}
