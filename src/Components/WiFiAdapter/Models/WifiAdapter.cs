﻿using src.ComponentDetails.WifiStandardVersions;
using src.Components.WiFiAdapter.Entities;

namespace src.Components.WiFiAdapter.Models;

public class WifiAdapter : IWifiAdapter
{
    public WifiAdapter(
        WifiStandardVersion wifiStandard,
        bool bluetoothModule,
        int pciVersion,
        int powerConsumption)
    {
        WifiStandard = wifiStandard;
        BluetoothModule = bluetoothModule;
        PciVersion = pciVersion;
        PowerConsumption = powerConsumption;
    }

    public WifiStandardVersion WifiStandard { get; }
    public bool BluetoothModule { get; }
    public int PciVersion { get; }
    public int PowerConsumption { get; }

    public string Name { get; } = "WifiAdapter";

    public IWifiAdapterBuilder Direct(IWifiAdapterBuilder builder)
    {
        builder.AddWifiStandardVersion(WifiStandard);
        builder.AddBluetoothModule(BluetoothModule);
        builder.AddPciVersion(PciVersion);
        builder.AddPowerConsumption(PowerConsumption);

        return builder;
    }
}
