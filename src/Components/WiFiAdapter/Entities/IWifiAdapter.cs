﻿using src.ComponentDetails.WifiStandardVersions;

namespace src.Components.WiFiAdapter.Entities;

public interface IWifiAdapter : IComponent, IWifiAdapterBuilderDirector
{
    public WifiStandardVersion WifiStandard { get; }
    public bool BluetoothModule { get; }
    public int PciVersion { get; }
    public int PowerConsumption { get; }
}
