﻿namespace src.Components.WiFiAdapter.Entities;

public interface IWifiAdapterBuilderDirector
{
    IWifiAdapterBuilder Direct(IWifiAdapterBuilder builder);
}
