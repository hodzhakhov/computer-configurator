﻿using src.ComponentDetails.WifiStandardVersions;

namespace src.Components.WiFiAdapter.Entities;

public interface IWifiAdapterBuilder
{
    IWifiAdapterBuilder AddWifiStandardVersion(WifiStandardVersion wifiStandard);
    IWifiAdapterBuilder AddBluetoothModule(bool bluetoothModule);
    IWifiAdapterBuilder AddPciVersion(int pciVersion);
    IWifiAdapterBuilder AddPowerConsumption(int powerConsumption);
    IWifiAdapter Build();
}
