﻿namespace src.Components.PowerUnit.Entities;

public interface IPowerUnitBuilderDirector
{
    IPowerUnitBuilder Direct(IPowerUnitBuilder builder);
}
