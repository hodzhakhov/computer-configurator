﻿using src.Results;

namespace src.Components.PowerUnit.Entities;

public interface IPowerUnit : IComponent, IPowerUnitBuilderDirector
{
    public int MaxPowerConsumption { get; }
    public ValidateResultComponent CheckPowerConsumption(int powerConsumption);
}
