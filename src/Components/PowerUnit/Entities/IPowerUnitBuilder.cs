﻿namespace src.Components.PowerUnit.Entities;

public interface IPowerUnitBuilder
{
    IPowerUnitBuilder AddMaxPowerConsumption(int maxPowerConsumption);
    IPowerUnit Build();
}
