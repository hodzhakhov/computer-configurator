﻿using src.Components.PowerUnit.Entities;

namespace src.Components.PowerUnit.Models;

public class PowerUnitBuilder : IPowerUnitBuilder
{
    private int _maxPowerConsumption;

    public IPowerUnitBuilder AddMaxPowerConsumption(int maxPowerConsumption)
    {
        _maxPowerConsumption = maxPowerConsumption;
        return this;
    }

    public IPowerUnit Build()
    {
        return new PowerUnit(_maxPowerConsumption);
    }
}
