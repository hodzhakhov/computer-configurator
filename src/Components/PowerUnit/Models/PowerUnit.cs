﻿using src.Components.PowerUnit.Entities;
using src.Results;

namespace src.Components.PowerUnit.Models;

public class PowerUnit : IPowerUnit
{
    private const double PowerRatio = 0.8;

    public PowerUnit(int maxPowerConsumption)
    {
        MaxPowerConsumption = maxPowerConsumption;
    }

    public int MaxPowerConsumption { get; }

    public string Name { get; } = "PowerUnit";

    public IPowerUnitBuilder Direct(IPowerUnitBuilder builder)
    {
        builder.AddMaxPowerConsumption(MaxPowerConsumption);

        return builder;
    }

    public ValidateResultComponent CheckPowerConsumption(int powerConsumption)
    {
        if (powerConsumption > MaxPowerConsumption)
        {
            return new ValidateResultComponent.FailedValidateResultComponent(
                "Power unit doesn't support so many power consumption");
        }

        if (powerConsumption > MaxPowerConsumption * PowerRatio)
        {
            return new ValidateResultComponent.WarningValidateResultComponent(
                "Power consumption exceeded the recommended interval");
        }

        return new ValidateResultComponent.SuccessValidateResultComponent();
    }
}
