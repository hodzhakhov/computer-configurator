﻿namespace src.Components.CPU.Entities;

public interface ICpuBuilderDirector
{
    ICpuBuilder Direct(ICpuBuilder builder);
}
