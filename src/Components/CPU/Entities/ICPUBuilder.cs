﻿using System.Collections.Generic;
using src.ComponentDetails.Sockets;

namespace src.Components.CPU.Entities;

public interface ICpuBuilder
{
    ICpuBuilder AddCoreFrequency(int coreFrequency);
    ICpuBuilder AddCoreAmount(int coreAmount);
    ICpuBuilder AddSocket(Socket socket);
    ICpuBuilder AddVideoCore(bool videoCore);
    ICpuBuilder AddMemoryFrequencies(IEnumerable<int> supportedFrequencies);
    ICpuBuilder AddTdp(int tdp);
    ICpuBuilder AddPowerConsumption(int powerConsumption);
    ICpu Build();
}
