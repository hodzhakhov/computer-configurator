﻿using System.Collections.Generic;
using src.ComponentDetails.Sockets;
using src.Components.GPU.Entities;
using src.Results;

namespace src.Components.CPU.Entities;

public interface ICpu : IComponent, ICpuBuilderDirector
{
    public int CoreFrequency { get; }
    public int CoreAmount { get; }
    public Socket Socket { get; }
    public bool VideoCore { get; }
    public IReadOnlyList<int> SupportedFrequencies { get; }
    public int Tdp { get; }
    public int PowerConsumption { get; }
    public ValidateResultComponent CheckGpu(IGpu? gpu);
}
