﻿using System;
using System.Collections.Generic;
using System.Linq;
using src.ComponentDetails.Sockets;
using src.Components.CPU.Entities;

namespace src.Components.CPU.Models;

public class CpuBuilder : ICpuBuilder
{
    private int _coreFrequency;
    private int _coreAmount;
    private Socket? _socket;
    private bool _videoCore;
    private IReadOnlyList<int>? _supportedFrequencies;
    private int _tdp;
    private int _powerConsumption;

    public ICpuBuilder AddCoreFrequency(int coreFrequency)
    {
        _coreFrequency = coreFrequency;
        return this;
    }

    public ICpuBuilder AddCoreAmount(int coreAmount)
    {
        _coreAmount = coreAmount;
        return this;
    }

    public ICpuBuilder AddSocket(Socket socket)
    {
        _socket = socket;
        return this;
    }

    public ICpuBuilder AddVideoCore(bool videoCore)
    {
        _videoCore = videoCore;
        return this;
    }

    public ICpuBuilder AddMemoryFrequencies(IEnumerable<int> supportedFrequencies)
    {
        _supportedFrequencies = supportedFrequencies.ToList();
        return this;
    }

    public ICpuBuilder AddTdp(int tdp)
    {
        _tdp = tdp;
        return this;
    }

    public ICpuBuilder AddPowerConsumption(int powerConsumption)
    {
        _powerConsumption = powerConsumption;
        return this;
    }

    public ICpu Build()
    {
        return new Cpu(
            _coreFrequency,
            _coreAmount,
            _socket ?? throw new ArgumentNullException(nameof(_socket)),
            _videoCore,
            _supportedFrequencies ?? throw new ArgumentNullException(nameof(_supportedFrequencies)),
            _tdp,
            _powerConsumption);
    }
}
