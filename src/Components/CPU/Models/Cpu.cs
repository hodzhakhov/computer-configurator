﻿using System.Collections.Generic;
using System.Linq;
using src.ComponentDetails.Sockets;
using src.Components.CPU.Entities;
using src.Components.GPU.Entities;
using src.Results;

namespace src.Components.CPU.Models;

public class Cpu : ICpu
{
    public Cpu(
        int coreFrequency,
        int coreAmount,
        Socket socket,
        bool videoCore,
        IEnumerable<int> supportedFrequencies,
        int tdp,
        int powerConsumption)
    {
        CoreFrequency = coreFrequency;
        CoreAmount = coreAmount;
        Socket = socket;
        VideoCore = videoCore;
        SupportedFrequencies = supportedFrequencies.ToList();
        Tdp = tdp;
        PowerConsumption = powerConsumption;
    }

    public int CoreFrequency { get; }
    public int CoreAmount { get; }
    public Socket Socket { get; }
    public bool VideoCore { get; }
    public IReadOnlyList<int> SupportedFrequencies { get; }
    public int Tdp { get; }
    public int PowerConsumption { get; }

    public string Name { get; } = "Cpu";

    public ICpuBuilder Direct(ICpuBuilder builder)
    {
        builder.AddCoreFrequency(CoreFrequency);
        builder.AddCoreAmount(CoreAmount);
        builder.AddSocket(Socket);
        builder.AddVideoCore(VideoCore);
        builder.AddMemoryFrequencies(SupportedFrequencies);
        builder.AddTdp(Tdp);
        builder.AddPowerConsumption(PowerConsumption);

        return builder;
    }

    public ValidateResultComponent CheckGpu(IGpu? gpu)
    {
        if (gpu is null && !VideoCore)
        {
            return new ValidateResultComponent.FailedValidateResultComponent("There is no video core and gpu");
        }

        return new ValidateResultComponent.SuccessValidateResultComponent();
    }
}
