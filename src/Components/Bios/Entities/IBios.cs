﻿using System.Collections.Generic;
using src.Components.CPU.Entities;

namespace src.Components.Bios.Entities;

public interface IBios : IComponent, IBiosBuilderDirector
{
    public string Type { get; }
    public string Version { get; }
    public IReadOnlyList<ICpu> CpuList { get; }
}
