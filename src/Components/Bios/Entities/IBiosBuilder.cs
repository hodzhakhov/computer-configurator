﻿using System.Collections.Generic;
using src.Components.CPU.Entities;

namespace src.Components.Bios.Entities;

public interface IBiosBuilder
{
    IBiosBuilder AddType(string type);
    IBiosBuilder AddVersion(string version);
    IBiosBuilder AddCpuList(IEnumerable<ICpu> cpuList);
    IBios Build();
}
