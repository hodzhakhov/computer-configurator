﻿namespace src.Components.Bios.Entities;

public interface IBiosBuilderDirector
{
    IBiosBuilder Direct(IBiosBuilder builder);
}
