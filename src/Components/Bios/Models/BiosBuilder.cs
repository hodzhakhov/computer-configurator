﻿using System;
using System.Collections.Generic;
using System.Linq;
using src.Components.Bios.Entities;
using src.Components.CPU.Entities;

namespace src.Components.Bios.Models;

public class BiosBuilder : IBiosBuilder
{
    private string? _type;
    private string? _version;
    private IReadOnlyList<ICpu>? _cpuList;

    public IBiosBuilder AddType(string type)
    {
        _type = type;
        return this;
    }

    public IBiosBuilder AddVersion(string version)
    {
        _version = version;
        return this;
    }

    public IBiosBuilder AddCpuList(IEnumerable<ICpu> cpuList)
    {
        _cpuList = cpuList.ToList();
        return this;
    }

    public IBios Build()
    {
        return new Bios(
            _type ?? throw new ArgumentNullException(nameof(_type)),
            _version ?? throw new ArgumentNullException(nameof(_version)),
            _cpuList ?? throw new ArgumentNullException(nameof(_cpuList)));
    }
}
