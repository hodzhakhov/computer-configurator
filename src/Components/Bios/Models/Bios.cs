﻿using System.Collections.Generic;
using System.Linq;
using src.Components.Bios.Entities;
using src.Components.CPU.Entities;

namespace src.Components.Bios.Models;

public class Bios : IBios
{
    public Bios(
        string type,
        string version,
        IEnumerable<ICpu> cpuList)
    {
        Type = type;
        Version = version;
        CpuList = cpuList.ToList();
    }

    public string Type { get; }
    public string Version { get; }
    public IReadOnlyList<ICpu> CpuList { get; }

    public string Name { get; } = "Bios";

    public IBiosBuilder Direct(IBiosBuilder builder)
    {
        builder.AddType(Type);
        builder.AddVersion(Version);
        builder.AddCpuList(CpuList);

        return builder;
    }
}
