﻿using System.Collections.Generic;
using src.ComponentDetails.DdrStandards;
using src.ComponentDetails.FormFactors;
using src.ComponentDetails.JedecVoltagePairs;
using src.Components.XMP.Models;

namespace src.Components.RAM.Entities;

public interface IRam : IComponent, IRamBuilderDirector
{
    public int Capacity { get; }
    public int Amount { get; }
    public IReadOnlyList<JedecVoltagePair> JedecVoltagePairs { get; }
    public IReadOnlyList<Xmp> XmpProfiles { get; }
    public RamFormFactor FormFactor { get; }
    public DdrStandard DdrStandard { get; }
    public int PowerConsumption { get; }
}
