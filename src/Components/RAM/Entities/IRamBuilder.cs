﻿using System.Collections.Generic;
using src.ComponentDetails.DdrStandards;
using src.ComponentDetails.FormFactors;
using src.ComponentDetails.JedecVoltagePairs;
using src.Components.XMP.Models;

namespace src.Components.RAM.Entities;

public interface IRamBuilder
{
    IRamBuilder AddCapacity(int capacity);
    IRamBuilder AddAmount(int amount);
    IRamBuilder AddJedecAndVoltage(IEnumerable<JedecVoltagePair> jedecVoltagePairs);
    IRamBuilder AddXmpProfiles(IEnumerable<Xmp> xmpProfiles);
    IRamBuilder AddFormFactor(RamFormFactor formFactor);
    IRamBuilder AddDdrStandard(DdrStandard ddrStandard);
    IRamBuilder AddPowerConsumption(int powerConsumption);
    IRam Build();
}
