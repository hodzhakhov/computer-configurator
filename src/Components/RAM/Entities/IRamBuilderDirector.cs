﻿namespace src.Components.RAM.Entities;

public interface IRamBuilderDirector
{
    IRamBuilder Direct(IRamBuilder builder);
}
