﻿using System.Collections.Generic;
using System.Linq;
using src.ComponentDetails.DdrStandards;
using src.ComponentDetails.FormFactors;
using src.ComponentDetails.JedecVoltagePairs;
using src.Components.RAM.Entities;
using src.Components.XMP.Models;

namespace src.Components.RAM.Models;

public class Ram : IRam
{
    public Ram(
        int capacity,
        int amount,
        IEnumerable<JedecVoltagePair> jedecVoltagePairs,
        IEnumerable<Xmp> xmpProfiles,
        RamFormFactor formFactor,
        DdrStandard ddrStandard,
        int powerConsumption)
    {
        Capacity = capacity;
        Amount = amount;
        JedecVoltagePairs = jedecVoltagePairs.ToList();
        XmpProfiles = xmpProfiles.ToList();
        FormFactor = formFactor;
        DdrStandard = ddrStandard;
        PowerConsumption = powerConsumption;
    }

    public int Capacity { get; }
    public int Amount { get; }
    public IReadOnlyList<JedecVoltagePair> JedecVoltagePairs { get; }
    public IReadOnlyList<Xmp> XmpProfiles { get; }
    public RamFormFactor FormFactor { get; }
    public DdrStandard DdrStandard { get; }
    public int PowerConsumption { get; }

    public string Name { get; } = "Ram";

    public IRamBuilder Direct(IRamBuilder builder)
    {
        builder.AddCapacity(Capacity);
        builder.AddAmount(Amount);
        builder.AddJedecAndVoltage(JedecVoltagePairs);
        builder.AddXmpProfiles(XmpProfiles);
        builder.AddFormFactor(FormFactor);
        builder.AddDdrStandard(DdrStandard);
        builder.AddPowerConsumption(PowerConsumption);

        return builder;
    }
}
