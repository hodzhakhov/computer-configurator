﻿using System;
using System.Collections.Generic;
using System.Linq;
using src.ComponentDetails.DdrStandards;
using src.ComponentDetails.FormFactors;
using src.ComponentDetails.JedecVoltagePairs;
using src.Components.RAM.Entities;
using src.Components.XMP.Models;

namespace src.Components.RAM.Models;

public class RamBuilder : IRamBuilder
{
    private int _capacity;
    private int _amount;
    private IReadOnlyList<JedecVoltagePair>? _jedecVoltagePairs;
    private IReadOnlyList<Xmp>? _xmpProfiles;
    private RamFormFactor? _formFactor;
    private DdrStandard? _ddrStandard;
    private int _powerConsumption;

    public IRamBuilder AddCapacity(int capacity)
    {
        _capacity = capacity;
        return this;
    }

    public IRamBuilder AddAmount(int amount)
    {
        _amount = amount;
        return this;
    }

    public IRamBuilder AddJedecAndVoltage(IEnumerable<JedecVoltagePair> jedecVoltagePairs)
    {
        _jedecVoltagePairs = jedecVoltagePairs.ToList();
        return this;
    }

    public IRamBuilder AddXmpProfiles(IEnumerable<Xmp> xmpProfiles)
    {
        _xmpProfiles = xmpProfiles.ToList();
        return this;
    }

    public IRamBuilder AddFormFactor(RamFormFactor formFactor)
    {
        _formFactor = formFactor;
        return this;
    }

    public IRamBuilder AddDdrStandard(DdrStandard ddrStandard)
    {
        _ddrStandard = ddrStandard;
        return this;
    }

    public IRamBuilder AddPowerConsumption(int powerConsumption)
    {
        _powerConsumption = powerConsumption;
        return this;
    }

    public IRam Build()
    {
        return new Ram(
            _capacity,
            _amount,
            _jedecVoltagePairs ?? throw new ArgumentNullException(nameof(_jedecVoltagePairs)),
            _xmpProfiles ?? throw new ArgumentNullException(nameof(_xmpProfiles)),
            _formFactor ?? throw new ArgumentNullException(nameof(_formFactor)),
            _ddrStandard ?? throw new ArgumentNullException(nameof(_ddrStandard)),
            _powerConsumption);
    }
}
