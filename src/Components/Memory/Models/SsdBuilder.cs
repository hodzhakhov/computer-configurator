﻿using System;
using src.ComponentDetails.Connectors;
using src.Components.Memory.Entities;

namespace src.Components.Memory.Models;

public class SsdBuilder : ISsdBuilder
{
    private ConnectorType? _connector;
    private int _capacity;
    private int _maxSpeed;
    private int _powerConsumption;

    public ISsdBuilder AddConnection(ConnectorType connector)
    {
        _connector = connector;
        return this;
    }

    public ISsdBuilder AddCapacity(int capacity)
    {
        _capacity = capacity;
        return this;
    }

    public ISsdBuilder AddMaxSpeed(int maxSpeed)
    {
        _maxSpeed = maxSpeed;
        return this;
    }

    public ISsdBuilder AddPowerConsumption(int powerConsumption)
    {
        _powerConsumption = powerConsumption;
        return this;
    }

    public ISsd Build()
    {
        return new Ssd(
            _connector ?? throw new ArgumentNullException(nameof(_connector)),
            _capacity,
            _maxSpeed,
            _powerConsumption);
    }
}
