﻿using src.Components.Memory.Entities;

namespace src.Components.Memory.Models;

public class Hdd : IHdd
{
    public Hdd(
        int capacity,
        int rotatingSpeed,
        int powerConsumption)
    {
        Capacity = capacity;
        RotatingSpeed = rotatingSpeed;
        PowerConsumption = powerConsumption;
    }

    public int Capacity { get; }
    public int RotatingSpeed { get; }
    public int PowerConsumption { get; }

    public string Name { get; } = "Hdd";

    public IHddBuilder Direct(IHddBuilder builder)
    {
        builder.AddCapacity(Capacity);
        builder.AddRotatingSpeed(RotatingSpeed);
        builder.AddPowerConsumption(PowerConsumption);

        return builder;
    }
}
