﻿using src.Components.Memory.Entities;

namespace src.Components.Memory.Models;

public class HddBuilder : IHddBuilder
{
    private int _capacity;
    private int _rotatingSpeed;
    private int _powerConsumption;

    public IHddBuilder AddCapacity(int capacity)
    {
        _capacity = capacity;
        return this;
    }

    public IHddBuilder AddRotatingSpeed(int rotatingSpeed)
    {
        _rotatingSpeed = rotatingSpeed;
        return this;
    }

    public IHddBuilder AddPowerConsumption(int powerConsumption)
    {
        _powerConsumption = powerConsumption;
        return this;
    }

    public IHdd Build()
    {
        return new Hdd(
            _capacity,
            _rotatingSpeed,
            _powerConsumption);
    }
}
