﻿using src.ComponentDetails.Connectors;
using src.Components.Memory.Entities;

namespace src.Components.Memory.Models;

public class Ssd : ISsd
{
    public Ssd(
        ConnectorType connector,
        int capacity,
        int maxSpeed,
        int powerConsumption)
    {
        Connector = connector;
        Capacity = capacity;
        MaxSpeed = maxSpeed;
        PowerConsumption = powerConsumption;
    }

    public ConnectorType Connector { get; }
    public int Capacity { get; }
    public int MaxSpeed { get; }
    public int PowerConsumption { get; }

    public string Name { get; } = "Ssd";

    public ISsdBuilder Direct(ISsdBuilder builder)
    {
        builder.AddConnection(Connector);
        builder.AddCapacity(Capacity);
        builder.AddMaxSpeed(MaxSpeed);
        builder.AddPowerConsumption(PowerConsumption);

        return builder;
    }
}
