﻿namespace src.Components.Memory.Entities;

public interface IMemory : IComponent
{
    public int Capacity { get; }

    public int PowerConsumption { get; }
}
