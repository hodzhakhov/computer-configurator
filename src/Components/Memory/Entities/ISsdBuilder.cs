﻿using src.ComponentDetails.Connectors;

namespace src.Components.Memory.Entities;

public interface ISsdBuilder
{
    ISsdBuilder AddConnection(ConnectorType connector);
    ISsdBuilder AddCapacity(int capacity);
    ISsdBuilder AddMaxSpeed(int maxSpeed);
    ISsdBuilder AddPowerConsumption(int powerConsumption);
    ISsd Build();
}
