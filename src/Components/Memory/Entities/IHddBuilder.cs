﻿namespace src.Components.Memory.Entities;

public interface IHddBuilder
{
    IHddBuilder AddCapacity(int capacity);
    IHddBuilder AddRotatingSpeed(int rotatingSpeed);
    IHddBuilder AddPowerConsumption(int powerConsumption);
    IHdd Build();
}
