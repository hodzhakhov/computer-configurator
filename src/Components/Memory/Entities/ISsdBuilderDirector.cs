﻿namespace src.Components.Memory.Entities;

public interface ISsdBuilderDirector
{
    ISsdBuilder Direct(ISsdBuilder builder);
}
