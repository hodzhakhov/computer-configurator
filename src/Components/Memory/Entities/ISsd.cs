﻿using src.ComponentDetails.Connectors;

namespace src.Components.Memory.Entities;

public interface ISsd : IMemory, ISsdBuilderDirector
{
    public ConnectorType Connector { get; }
    public int MaxSpeed { get; }
}
