﻿namespace src.Components.Memory.Entities;

public interface IHdd : IMemory, IHddBuilderDirector
{
    public int RotatingSpeed { get; }
}
