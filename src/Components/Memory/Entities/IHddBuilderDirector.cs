﻿namespace src.Components.Memory.Entities;

public interface IHddBuilderDirector
{
    IHddBuilder Direct(IHddBuilder builder);
}
