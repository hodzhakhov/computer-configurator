﻿using System.Collections.Generic;
using System.Linq;
using src.ComponentDetails.Sockets;
using src.Components.CoolingSystem.Entities;
using src.Components.CPU.Entities;
using src.Components.Motherboards.Entities;
using src.Results;

namespace src.Components.CoolingSystem.Models;

public class CoolingSystem : ICoolingSystem
{
    public CoolingSystem(
        int size,
        IEnumerable<Socket> sockets,
        int maxTdp)
    {
        Size = size;
        Sockets = sockets.ToList();
        MaxTdp = maxTdp;
    }

    public int Size { get; }
    public IReadOnlyList<Socket> Sockets { get; }
    public int MaxTdp { get; }

    public string Name { get; } = "CoolingSystem";

    public ICoolerBuilder Direct(ICoolerBuilder builder)
    {
        builder.AddSize(Size);
        builder.AddSockets(Sockets);
        builder.AddTdp(MaxTdp);

        return builder;
    }

    public ValidateResultComponent CheckCpu(ICpu cpu)
    {
        if (MaxTdp < cpu.Tdp)
        {
            return new ValidateResultComponent.WarningValidateResultComponent("CPU's TDP is bigger than max TDP of cooler");
        }

        if (!Sockets.Contains(cpu.Socket))
        {
            return new ValidateResultComponent.FailedValidateResultComponent("CPU's socket doesn't match cooler's socket");
        }

        return new ValidateResultComponent.SuccessValidateResultComponent();
    }

    public ValidateResultComponent CheckMotherBoard(IMotherBoard motherBoard)
    {
        if (!Sockets.Contains(motherBoard.Socket))
        {
            return new ValidateResultComponent.FailedValidateResultComponent("Motherboard's socket doesn't match cooler's socket");
        }

        return new ValidateResultComponent.SuccessValidateResultComponent();
    }
}
