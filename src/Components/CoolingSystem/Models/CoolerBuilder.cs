﻿using System;
using System.Collections.Generic;
using System.Linq;
using src.ComponentDetails.Sockets;
using src.Components.CoolingSystem.Entities;

namespace src.Components.CoolingSystem.Models;

public class CoolerBuilder : ICoolerBuilder
{
    private int _size;
    private IReadOnlyList<Socket>? _sockets;
    private int _maxTdp;

    public ICoolerBuilder AddSize(int size)
    {
        _size = size;
        return this;
    }

    public ICoolerBuilder AddSockets(IEnumerable<Socket> sockets)
    {
        _sockets = sockets.ToList();
        return this;
    }

    public ICoolerBuilder AddTdp(int maxTdp)
    {
        _maxTdp = maxTdp;
        return this;
    }

    public ICoolingSystem Build()
    {
        return new CoolingSystem(
            _size,
            _sockets ?? throw new ArgumentNullException(nameof(_sockets)),
            _maxTdp);
    }
}
