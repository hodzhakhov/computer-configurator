﻿namespace src.Components.CoolingSystem.Entities;

public interface ICoolerBuilderDirector
{
    ICoolerBuilder Direct(ICoolerBuilder builder);
}
