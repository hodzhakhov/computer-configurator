﻿using System.Collections.Generic;
using src.ComponentDetails.Sockets;
using src.Components.CPU.Entities;
using src.Components.Motherboards.Entities;
using src.Results;

namespace src.Components.CoolingSystem.Entities;

public interface ICoolingSystem : IComponent, ICoolerBuilderDirector
{
    public int Size { get; }
    public IReadOnlyList<Socket> Sockets { get; }
    public int MaxTdp { get; }
    public ValidateResultComponent CheckCpu(ICpu cpu);
    public ValidateResultComponent CheckMotherBoard(IMotherBoard motherBoard);
}
