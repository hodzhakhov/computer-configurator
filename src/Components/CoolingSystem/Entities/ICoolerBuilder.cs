﻿using System.Collections.Generic;
using src.ComponentDetails.Sockets;

namespace src.Components.CoolingSystem.Entities;

public interface ICoolerBuilder
{
    ICoolerBuilder AddSize(int size);
    ICoolerBuilder AddSockets(IEnumerable<Socket> sockets);
    ICoolerBuilder AddTdp(int maxTdp);
    ICoolingSystem Build();
}
