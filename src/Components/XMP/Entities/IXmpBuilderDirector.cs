﻿namespace src.Components.XMP.Entities;

public interface IXmpBuilderDirector
{
    IXmpBuilder Direct(IXmpBuilder builder);
}
