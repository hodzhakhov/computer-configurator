﻿using src.ComponentDetails.Timings;

namespace src.Components.XMP.Entities;

public interface IXmp : IComponent, IXmpBuilderDirector
{
    public Timing Timings { get; }
    public int Voltage { get; }
    public int Frequency { get; }
}
