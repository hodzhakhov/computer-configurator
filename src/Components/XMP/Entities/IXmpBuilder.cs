﻿using src.ComponentDetails.Timings;

namespace src.Components.XMP.Entities;

public interface IXmpBuilder
{
    IXmpBuilder AddTimings(Timing timings);
    IXmpBuilder AddVoltage(int voltage);
    IXmpBuilder AddFrequency(int frequency);
    IXmp Build();
}
