﻿using System;
using src.ComponentDetails.Timings;
using src.Components.XMP.Entities;

namespace src.Components.XMP.Models;

public class XmpBuilder : IXmpBuilder
{
    private Timing? _timings;
    private int _voltage;
    private int _frequency;

    public IXmpBuilder AddTimings(Timing timings)
    {
        _timings = timings;
        return this;
    }

    public IXmpBuilder AddVoltage(int voltage)
    {
        _voltage = voltage;
        return this;
    }

    public IXmpBuilder AddFrequency(int frequency)
    {
        _frequency = frequency;
        return this;
    }

    public IXmp Build()
    {
        return new Xmp(
            _timings ?? throw new ArgumentNullException(nameof(_timings)),
            _voltage,
            _frequency);
    }
}
