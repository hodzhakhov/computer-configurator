﻿using src.ComponentDetails.Timings;
using src.Components.XMP.Entities;

namespace src.Components.XMP.Models;

public class Xmp : IXmp
{
    public Xmp(
        Timing timings,
        int voltage,
        int frequency)
    {
        Timings = timings;
        Voltage = voltage;
        Frequency = frequency;
    }

    public Timing Timings { get; }
    public int Voltage { get; }
    public int Frequency { get; }

    public string Name { get; } = "Xmp";

    public IXmpBuilder Direct(IXmpBuilder builder)
    {
        builder.AddTimings(Timings);
        builder.AddVoltage(Voltage);
        builder.AddFrequency(Frequency);

        return builder;
    }
}
