﻿using src.Components.GPU.Entities;

namespace src.Components.GPU.Models;

public class GpuBuilder : IGpuBuilder
{
    private int _height;
    private int _width;
    private int _videoMemory;
    private int _pciVersion;
    private int _frequency;
    private int _powerConsumption;

    public IGpuBuilder AddHeight(int height)
    {
        _height = height;
        return this;
    }

    public IGpuBuilder AddWidth(int width)
    {
        _width = width;
        return this;
    }

    public IGpuBuilder AddVideoMemory(int videoMemory)
    {
        _videoMemory = videoMemory;
        return this;
    }

    public IGpuBuilder AddPciVersion(int pciVersion)
    {
        _pciVersion = pciVersion;
        return this;
    }

    public IGpuBuilder AddFrequency(int frequency)
    {
        _frequency = frequency;
        return this;
    }

    public IGpuBuilder AddPowerConsumption(int powerConsumption)
    {
        _powerConsumption = powerConsumption;
        return this;
    }

    public IGpu Build()
    {
        return new Gpu(
            _height,
            _width,
            _videoMemory,
            _pciVersion,
            _frequency,
            _powerConsumption);
    }
}
