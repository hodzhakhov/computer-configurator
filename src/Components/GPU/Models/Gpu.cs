﻿using src.Components.GPU.Entities;

namespace src.Components.GPU.Models;

public class Gpu : IGpu
{
    public Gpu(
        int height,
        int width,
        int videoMemory,
        int pciVersion,
        int frequency,
        int powerConsumption)
    {
        Height = height;
        Width = width;
        VideoMemory = videoMemory;
        PciVersion = pciVersion;
        Frequency = frequency;
        PowerConsumption = powerConsumption;
    }

    public int Height { get; }
    public int Width { get; }
    public int VideoMemory { get; }
    public int PciVersion { get; }
    public int Frequency { get; }
    public int PowerConsumption { get; }

    public string Name { get; } = "Gpu";

    public IGpuBuilder Direct(IGpuBuilder builder)
    {
        builder.AddHeight(Height);
        builder.AddWidth(Width);
        builder.AddVideoMemory(VideoMemory);
        builder.AddPciVersion(PciVersion);
        builder.AddFrequency(Frequency);
        builder.AddPowerConsumption(PowerConsumption);

        return builder;
    }
}
