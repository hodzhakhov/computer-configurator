﻿namespace src.Components.GPU.Entities;

public interface IGpuBuilder
{
    IGpuBuilder AddHeight(int height);
    IGpuBuilder AddWidth(int width);
    IGpuBuilder AddVideoMemory(int videoMemory);
    IGpuBuilder AddPciVersion(int pciVersion);
    IGpuBuilder AddFrequency(int frequency);
    IGpuBuilder AddPowerConsumption(int powerConsumption);
    IGpu Build();
}
