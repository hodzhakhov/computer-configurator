﻿namespace src.Components.GPU.Entities;

public interface IGpuBuilderDirector
{
    IGpuBuilder Direct(IGpuBuilder builder);
}
