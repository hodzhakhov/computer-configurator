﻿namespace src.Components.GPU.Entities;

public interface IGpu : IComponent, IGpuBuilderDirector
{
    public int Height { get; }
    public int Width { get; }
    public int VideoMemory { get; }
    public int PciVersion { get; }
    public int Frequency { get; }
    public int PowerConsumption { get; }
}
