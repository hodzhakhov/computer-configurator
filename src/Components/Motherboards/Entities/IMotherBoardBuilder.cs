﻿using src.ComponentDetails.Chipsets;
using src.ComponentDetails.DdrStandards;
using src.ComponentDetails.FormFactors;
using src.ComponentDetails.Sockets;
using src.Components.Bios.Entities;

namespace src.Components.Motherboards.Entities;

public interface IMotherBoardBuilder
{
    IMotherBoardBuilder AddSocket(Socket socket);
    IMotherBoardBuilder AddPciAmount(int pci);
    IMotherBoardBuilder AddSataAmount(int sata);
    IMotherBoardBuilder AddChipset(Chipset chipset);
    IMotherBoardBuilder AddDdr(DdrStandard ddr);
    IMotherBoardBuilder AddRamAmount(int ramAmount);
    IMotherBoardBuilder AddFormFactor(MotherBoardFormFactor formFactor);
    IMotherBoardBuilder AddBios(IBios? bios);
    IMotherBoard Build();
}
