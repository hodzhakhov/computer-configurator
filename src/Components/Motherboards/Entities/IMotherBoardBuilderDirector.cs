﻿namespace src.Components.Motherboards.Entities;

public interface IMotherBoardBuilderDirector
{
    IMotherBoardBuilder Direct(IMotherBoardBuilder builder);
}
