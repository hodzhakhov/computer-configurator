﻿using System.Collections.Generic;
using src.ComponentDetails.Chipsets;
using src.ComponentDetails.DdrStandards;
using src.ComponentDetails.FormFactors;
using src.ComponentDetails.Sockets;
using src.Components.Bios.Entities;
using src.Components.CPU.Entities;
using src.Components.Memory.Entities;
using src.Components.RAM.Entities;
using src.Results;

namespace src.Components.Motherboards.Entities;

public interface IMotherBoard : IComponent, IMotherBoardBuilderDirector
{
    public Socket Socket { get; }
    public int Pci { get; }
    public int Sata { get; }
    public Chipset Chipset { get; }
    public DdrStandard Ddr { get; }
    public int RamAmount { get; }
    public MotherBoardFormFactor FormFactor { get; }
    public IBios Bios { get; }
    public ValidateResultComponent CheckCpu(ICpu cpu);
    public ValidateResultComponent CheckRam(IRam ram);
    public ValidateResultComponent CheckSataMemory(IEnumerable<IMemory> memories);
    public ValidateResultComponent CheckPciMemory(IEnumerable<IMemory> memories);
    public ValidateResultComponent CheckFrequencies(ICpu cpu, IRam ram);
}
