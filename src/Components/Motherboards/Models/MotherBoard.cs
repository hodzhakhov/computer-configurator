﻿using System.Collections.Generic;
using System.Linq;
using src.ComponentDetails.Chipsets;
using src.ComponentDetails.Connectors;
using src.ComponentDetails.DdrStandards;
using src.ComponentDetails.FormFactors;
using src.ComponentDetails.Sockets;
using src.Components.Bios.Entities;
using src.Components.CPU.Entities;
using src.Components.Memory.Entities;
using src.Components.Motherboards.Entities;
using src.Components.RAM.Entities;
using src.Results;

namespace src.Components.Motherboards.Models;

public class MotherBoard : IMotherBoard
{
    public MotherBoard(
        Socket socket,
        int pci,
        int sata,
        Chipset chipset,
        DdrStandard ddr,
        int ramAmount,
        MotherBoardFormFactor formFactor,
        IBios bios)
    {
        Socket = socket;
        Pci = pci;
        Sata = sata;
        Chipset = chipset;
        Ddr = ddr;
        RamAmount = ramAmount;
        FormFactor = formFactor;
        Bios = bios;
    }

    public Socket Socket { get; }
    public int Pci { get; }
    public int Sata { get; }
    public Chipset Chipset { get; }
    public DdrStandard Ddr { get; }
    public int RamAmount { get; }
    public MotherBoardFormFactor FormFactor { get; }
    public IBios Bios { get; }

    public string Name { get; } = "MotherBoard";

    public IMotherBoardBuilder Direct(IMotherBoardBuilder builder)
    {
        builder.AddSocket(Socket);
        builder.AddPciAmount(Pci);
        builder.AddSataAmount(Sata);
        builder.AddChipset(Chipset);
        builder.AddDdr(Ddr);
        builder.AddRamAmount(RamAmount);
        builder.AddFormFactor(FormFactor);
        builder.AddBios(Bios);

        return builder;
    }

    public ValidateResultComponent CheckCpu(ICpu cpu)
    {
        if (Socket != cpu.Socket)
        {
            return new ValidateResultComponent.FailedValidateResultComponent(
                "CPU's socket doesn't match motherboard's socket");
        }

        if (!Bios.CpuList.Contains(cpu))
        {
            return new ValidateResultComponent.FailedValidateResultComponent("Cpu doesn't match bios");
        }

        return new ValidateResultComponent.SuccessValidateResultComponent();
    }

    public ValidateResultComponent CheckRam(IRam ram)
    {
        if (ram.DdrStandard != Ddr)
        {
            return new ValidateResultComponent.FailedValidateResultComponent(
                "Ram's DDR standard doesn't match motherboard's standard");
        }

        if (ram.Amount > RamAmount)
        {
            return new ValidateResultComponent.FailedValidateResultComponent(
                "Motherboard doesn't have so many RAM slots");
        }

        return new ValidateResultComponent.SuccessValidateResultComponent();
    }

    public ValidateResultComponent CheckSataMemory(IEnumerable<IMemory> memories)
    {
        int sataAmount = 0;
        foreach (IMemory memory in memories)
        {
            if (memory is ISsd ssd)
            {
                if (ssd.Connector is ConnectorType.SataConnector)
                {
                    ++sataAmount;
                }
            }
            else
            {
                ++sataAmount;
            }
        }

        if (sataAmount > Sata)
        {
            return new ValidateResultComponent.FailedValidateResultComponent(
                "Motherboard doesn't have enough quantity of Sata slots");
        }

        return new ValidateResultComponent.SuccessValidateResultComponent();
    }

    public ValidateResultComponent CheckPciMemory(IEnumerable<IMemory> memories)
    {
        int pciAmount = 0;
        foreach (IMemory memory in memories)
        {
            if (memory is ISsd { Connector: ConnectorType.PciConnector })
            {
                ++pciAmount;
            }
        }

        if (pciAmount > Sata)
        {
            return new ValidateResultComponent.FailedValidateResultComponent(
                "Motherboard doesn't have enough quantity of Pci slots");
        }

        return new ValidateResultComponent.SuccessValidateResultComponent();
    }

    public ValidateResultComponent CheckFrequencies(ICpu cpu, IRam ram)
    {
        var ramFrequencies = ram.JedecVoltagePairs.Select(pair => pair.Frequency).ToList();

        var ramXmpFrequencies = ram.XmpProfiles.Select(xmp => xmp.Frequency).ToList();

        var suitableRamFrequencies = Chipset.Frequency.Intersect(ramFrequencies).ToList();

        var suitableXmpRamFrequencies = new List<int>();

        if (Chipset.Xmp && ram.XmpProfiles.Count != 0)
        {
            suitableXmpRamFrequencies = Chipset.Frequency.Intersect(ramXmpFrequencies).ToList();
        }

        var cpuRamFrequencies = cpu.SupportedFrequencies.Intersect(suitableRamFrequencies).ToList();
        var cpuXmpRamFrequencies = cpu.SupportedFrequencies.Intersect(suitableXmpRamFrequencies).ToList();

        if (cpuRamFrequencies.Count != 0 || cpuXmpRamFrequencies.Count != 0)
        {
            return new ValidateResultComponent.SuccessValidateResultComponent();
        }

        return new ValidateResultComponent.FailedValidateResultComponent(
            "There are no common suitable frequencies for the motherboard, RAM and CPU");
    }
}
