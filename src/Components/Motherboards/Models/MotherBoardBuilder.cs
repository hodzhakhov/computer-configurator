﻿using System;
using src.ComponentDetails.Chipsets;
using src.ComponentDetails.DdrStandards;
using src.ComponentDetails.FormFactors;
using src.ComponentDetails.Sockets;
using src.Components.Bios.Entities;
using src.Components.Motherboards.Entities;

namespace src.Components.Motherboards.Models;

public class MotherBoardBuilder : IMotherBoardBuilder
{
    private Socket? _socket;
    private int _pci;
    private int _sata;
    private Chipset? _chipset;
    private DdrStandard? _ddr;
    private int _ram;
    private MotherBoardFormFactor? _formFactor;
    private IBios? _bios;

    public IMotherBoardBuilder AddSocket(Socket socket)
    {
        _socket = socket;
        return this;
    }

    public IMotherBoardBuilder AddPciAmount(int pci)
    {
        _pci = pci;
        return this;
    }

    public IMotherBoardBuilder AddSataAmount(int sata)
    {
        _sata = sata;
        return this;
    }

    public IMotherBoardBuilder AddChipset(Chipset chipset)
    {
        _chipset = chipset;
        return this;
    }

    public IMotherBoardBuilder AddDdr(DdrStandard ddr)
    {
        _ddr = ddr;
        return this;
    }

    public IMotherBoardBuilder AddRamAmount(int ramAmount)
    {
        _ram = ramAmount;
        return this;
    }

    public IMotherBoardBuilder AddFormFactor(MotherBoardFormFactor formFactor)
    {
        _formFactor = formFactor;
        return this;
    }

    public IMotherBoardBuilder AddBios(IBios? bios)
    {
        _bios = bios;
        return this;
    }

    public IMotherBoard Build()
    {
        return new MotherBoard(
            _socket ?? throw new ArgumentNullException(nameof(_socket)),
            _pci,
            _sata,
            _chipset ?? throw new ArgumentNullException(nameof(_chipset)),
            _ddr ?? throw new ArgumentNullException(nameof(_ddr)),
            _ram,
            _formFactor ?? throw new ArgumentNullException(nameof(_formFactor)),
            _bios ?? throw new ArgumentNullException(nameof(_bios)));
    }
}
