﻿namespace src.ComponentDetails.Sockets;

public record Socket(string Type);
