﻿namespace src.ComponentDetails.JedecVoltagePairs;

public record JedecVoltagePair(int Frequency, int Voltage);
