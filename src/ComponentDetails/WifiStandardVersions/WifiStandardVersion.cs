﻿namespace src.ComponentDetails.WifiStandardVersions;

public record WifiStandardVersion(string Version);
