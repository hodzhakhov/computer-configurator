﻿namespace src.ComponentDetails.Timings;

public record Timing(int TimingOne, int TimingTwo, int TimingThree, int TimingFour);
