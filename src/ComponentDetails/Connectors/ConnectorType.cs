﻿namespace src.ComponentDetails.Connectors;

public abstract record ConnectorType()
{
    public sealed record SataConnector() : ConnectorType;

    public sealed record PciConnector() : ConnectorType;
}
