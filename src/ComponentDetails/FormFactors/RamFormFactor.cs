﻿namespace src.ComponentDetails.FormFactors;

public record RamFormFactor(string FormFactor);
