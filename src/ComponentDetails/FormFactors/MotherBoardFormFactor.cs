﻿namespace src.ComponentDetails.FormFactors;

public record MotherBoardFormFactor(string FormFactor);
