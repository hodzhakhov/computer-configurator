﻿using System.Collections.Generic;

namespace src.ComponentDetails.Chipsets;

public record Chipset(IReadOnlyList<int> Frequency, bool Xmp);
