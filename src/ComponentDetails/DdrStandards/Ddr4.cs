﻿namespace src.ComponentDetails.DdrStandards;

public record Ddr4() : DdrStandard;
