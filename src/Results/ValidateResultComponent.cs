﻿namespace src.Results;

public abstract record ValidateResultComponent
{
    public sealed record SuccessValidateResultComponent() : ValidateResultComponent;

    public sealed record WarningValidateResultComponent(string Message) : ValidateResultComponent;

    public sealed record FailedValidateResultComponent(string Message) : ValidateResultComponent;
}
