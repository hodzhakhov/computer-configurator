﻿using System.Collections.Generic;
using src.Computer.Entities;

namespace src.Results;

public abstract record ValidateResultComputer
{
    public sealed record SuccessValidateResultComputer(IComputer Computer) : ValidateResultComputer;
    public sealed record SuccessWithWarningValidateResultComputer(IComputer Computer, IEnumerable<ValidateResultComponent> Message) : ValidateResultComputer;

    public sealed record FailedValidateResultComputer(IEnumerable<ValidateResultComponent> Message) : ValidateResultComputer;
}
