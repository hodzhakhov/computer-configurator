﻿namespace src;

public interface IComponent
{
    public string Name { get; }
}
