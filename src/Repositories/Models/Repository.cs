﻿using System.Collections.Generic;
using src.Repositories.Entities;

namespace src.Repositories.Models;

public class Repository : IRepository
{
    private Dictionary<int, IComponent> _components = new Dictionary<int, IComponent>();

    public void Add(int id, IComponent component)
    {
        _components.Add(id, component);
    }

    public IComponent? GetById(int id)
    {
        if (_components.TryGetValue(id, out IComponent? byId))
        {
            return byId;
        }

        return null;
    }
}
