﻿namespace src.Repositories.Entities;

public interface IRepository
{
    void Add(int id, IComponent component);
    IComponent? GetById(int id);
}
