﻿namespace src.Computer.Entities;

public interface IComputerBuilderDirector
{
    IComputerBuilder Direct(IComputerBuilder builder);
}
