﻿using src.Components.CoolingSystem.Entities;
using src.Components.CPU.Entities;
using src.Components.GPU.Entities;
using src.Components.Hull.Entities;
using src.Components.Memory.Entities;
using src.Components.Motherboards.Entities;
using src.Components.PowerUnit.Entities;
using src.Components.RAM.Entities;
using src.Components.WiFiAdapter.Entities;
using src.Results;

namespace src.Computer.Entities;

public interface IComputerBuilder
{
    IComputerBuilder AddMotherBoard(IMotherBoard? motherBoard);
    IComputerBuilder AddCpu(ICpu? cpu);
    IComputerBuilder AddCooler(ICoolingSystem? coolingSystem);
    IComputerBuilder AddRam(IRam? ram);
    IComputerBuilder AddMemory(IMemory? memory);
    IComputerBuilder AddHull(IHull? hull);
    IComputerBuilder AddPowerUnit(IPowerUnit? powerUnit);
    IComputerBuilder AddGpu(IGpu? gpu);
    IComputerBuilder AddWiFi(IWifiAdapter? wifiAdapter);
    ValidateResultComputer Build();
}
