﻿using System;
using System.Collections.Generic;
using System.Linq;
using src.Components.CoolingSystem.Entities;
using src.Components.CPU.Entities;
using src.Components.GPU.Entities;
using src.Components.Hull.Entities;
using src.Components.Memory.Entities;
using src.Components.Motherboards.Entities;
using src.Components.PowerUnit.Entities;
using src.Components.RAM.Entities;
using src.Components.WiFiAdapter.Entities;
using src.Computer.Entities;
using src.Results;

namespace src.Computer.Models;

public class ComputerBuilder : IComputerBuilder
{
    private IMotherBoard? _motherBoard;
    private ICpu? _cpu;
    private ICoolingSystem? _coolingSystem;
    private IRam? _ram;
    private IEnumerable<IMemory> _memory = new List<IMemory>();
    private IHull? _hull;
    private IPowerUnit? _powerUnit;
    private IGpu? _gpu;
    private IWifiAdapter? _wifiAdapter;
    private int _powerConsumption;

    public IComputerBuilder AddMotherBoard(IMotherBoard? motherBoard)
    {
        _motherBoard = motherBoard;
        return this;
    }

    public IComputerBuilder AddCpu(ICpu? cpu)
    {
        _cpu = cpu;
        if (cpu != null)
            _powerConsumption += cpu.PowerConsumption;
        return this;
    }

    public IComputerBuilder AddCooler(ICoolingSystem? coolingSystem)
    {
        _coolingSystem = coolingSystem;
        return this;
    }

    public IComputerBuilder AddRam(IRam? ram)
    {
        _ram = ram;
        if (ram != null)
            _powerConsumption += ram.PowerConsumption;
        return this;
    }

    public IComputerBuilder AddMemory(IMemory? memory)
    {
        if (memory != null)
        {
            _memory = _memory.Append(memory);
            _powerConsumption += memory.PowerConsumption;
        }

        return this;
    }

    public IComputerBuilder AddHull(IHull? hull)
    {
        _hull = hull;
        return this;
    }

    public IComputerBuilder AddPowerUnit(IPowerUnit? powerUnit)
    {
        _powerUnit = powerUnit;
        return this;
    }

    public IComputerBuilder AddGpu(IGpu? gpu)
    {
        _gpu = gpu;
        if (gpu != null)
            _powerConsumption += gpu.PowerConsumption;
        return this;
    }

    public IComputerBuilder AddWiFi(IWifiAdapter? wifiAdapter)
    {
        _wifiAdapter = wifiAdapter;
        if (wifiAdapter != null)
            _powerConsumption += wifiAdapter.PowerConsumption;
        return this;
    }

    public ValidateResultComputer Build()
    {
        if (_motherBoard is null || _cpu is null || _coolingSystem is null || _ram is null || _hull is null ||
            !_memory.Any() || _powerUnit is null)
        {
            throw new ArgumentNullException();
        }

        var validationResults = new List<ValidateResultComponent>();

        validationResults.Add(_motherBoard.CheckCpu(_cpu));
        validationResults.Add(_motherBoard.CheckPciMemory(_memory));
        validationResults.Add(_motherBoard.CheckSataMemory(_memory));
        validationResults.Add(_motherBoard.CheckRam(_ram));
        validationResults.Add(_motherBoard.CheckFrequencies(_cpu, _ram));
        validationResults.Add(_cpu.CheckGpu(_gpu));
        validationResults.Add(_coolingSystem.CheckCpu(_cpu));
        validationResults.Add(_coolingSystem.CheckMotherBoard(_motherBoard));
        validationResults.Add(_hull.CheckGpu(_gpu));
        validationResults.Add(_hull.CheckMotherBoard(_motherBoard));
        validationResults.Add(_hull.CheckCooler(_coolingSystem));
        validationResults.Add(_powerUnit.CheckPowerConsumption(_powerConsumption));

        var warningResult = new List<ValidateResultComponent>();
        var failedResult = new List<ValidateResultComponent>();

        foreach (ValidateResultComponent result in validationResults)
        {
            switch (result)
            {
                case ValidateResultComponent.WarningValidateResultComponent:
                    warningResult.Add(result);
                    break;
                case ValidateResultComponent.FailedValidateResultComponent:
                    failedResult.Add(result);
                    break;
            }
        }

        if (failedResult.Count != 0)
        {
            return new ValidateResultComputer.FailedValidateResultComputer(failedResult);
        }

        var computer = new Computer(
            _motherBoard,
            _cpu,
            _coolingSystem,
            _ram,
            _memory,
            _hull,
            _powerUnit,
            _gpu,
            _wifiAdapter);

        if (warningResult.Count != 0)
        {
            return new ValidateResultComputer.SuccessWithWarningValidateResultComputer(computer, warningResult);
        }

        return new ValidateResultComputer.SuccessValidateResultComputer(computer);
    }
}
