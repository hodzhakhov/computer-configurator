﻿using System.Collections.Generic;
using System.Linq;
using src.Components.CoolingSystem.Entities;
using src.Components.CPU.Entities;
using src.Components.GPU.Entities;
using src.Components.Hull.Entities;
using src.Components.Memory.Entities;
using src.Components.Motherboards.Entities;
using src.Components.PowerUnit.Entities;
using src.Components.RAM.Entities;
using src.Components.WiFiAdapter.Entities;
using src.Computer.Entities;

namespace src.Computer.Models;

public class Computer : IComputer
{
    private readonly IMotherBoard _motherBoard;
    private readonly ICpu _cpu;
    private readonly ICoolingSystem _coolingSystem;
    private readonly IRam _ram;
    private readonly IReadOnlyList<IMemory> _memory;
    private readonly IHull _hull;
    private readonly IPowerUnit _powerUnit;
    private readonly IGpu? _gpu;
    private readonly IWifiAdapter? _wifiAdapter;

    public Computer(
        IMotherBoard motherBoard,
        ICpu cpu,
        ICoolingSystem coolingSystem,
        IRam ram,
        IEnumerable<IMemory> memory,
        IHull hull,
        IPowerUnit powerUnit,
        IGpu? gpu,
        IWifiAdapter? wifiAdapter)
    {
        _motherBoard = motherBoard;
        _cpu = cpu;
        _coolingSystem = coolingSystem;
        _ram = ram;
        _memory = memory.ToList();
        _hull = hull;
        _powerUnit = powerUnit;
        _gpu = gpu;
        _wifiAdapter = wifiAdapter;
    }

    public IComputerBuilder Direct(IComputerBuilder builder)
    {
        builder.AddMotherBoard(_motherBoard);
        builder.AddCpu(_cpu);
        builder.AddCooler(_coolingSystem);
        builder.AddRam(_ram);

        foreach (IMemory memory in _memory)
        {
            builder.AddMemory(memory);
        }

        builder.AddHull(_hull);
        builder.AddPowerUnit(_powerUnit);

        if (_gpu is not null)
            builder.AddGpu(_gpu);

        if (_wifiAdapter is not null)
            builder.AddWiFi(_wifiAdapter);

        return builder;
    }
}
