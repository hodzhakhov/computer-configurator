﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using src.ComponentDetails.Chipsets;
using src.ComponentDetails.Connectors;
using src.ComponentDetails.DdrStandards;
using src.ComponentDetails.FormFactors;
using src.ComponentDetails.JedecVoltagePairs;
using src.ComponentDetails.Sockets;
using src.ComponentDetails.Timings;
using src.ComponentDetails.WifiStandardVersions;
using src.Components.Bios.Entities;
using src.Components.Bios.Models;
using src.Components.CoolingSystem.Entities;
using src.Components.CoolingSystem.Models;
using src.Components.CPU.Entities;
using src.Components.CPU.Models;
using src.Components.GPU.Entities;
using src.Components.GPU.Models;
using src.Components.Hull.Entities;
using src.Components.Hull.Models;
using src.Components.Memory.Entities;
using src.Components.Memory.Models;
using src.Components.Motherboards.Entities;
using src.Components.Motherboards.Models;
using src.Components.PowerUnit.Entities;
using src.Components.PowerUnit.Models;
using src.Components.RAM.Entities;
using src.Components.RAM.Models;
using src.Components.WiFiAdapter.Entities;
using src.Components.WiFiAdapter.Models;
using src.Components.XMP.Models;
using src.Computer.Models;
using src.Repositories.Models;
using src.Results;
using Xunit;

namespace tests.Tests;

public class ComputerTests
{
    [Fact]
    public void ComputerWithoutWarnings()
    {
        // Arrange
        var repository = new Repository();

        var cpuBuilder = new CpuBuilder();
        cpuBuilder.AddCoreFrequency(4400).AddSocket(new Socket("AM4")).AddTdp(100).AddCoreAmount(4)
            .AddMemoryFrequencies(new List<int> { 4400, 3300 }).AddVideoCore(true).AddPowerConsumption(100);
        ICpu cpu = cpuBuilder.Build();
        repository.Add(1, cpu);

        var biosBuilder = new BiosBuilder();
        biosBuilder.AddType("BIOS").AddVersion("1.1").AddCpuList(new List<ICpu>
        {
            repository.GetById(1) as ICpu ?? throw new InvalidDataException(),
        });

        IBios bios = biosBuilder.Build();
        repository.Add(2, bios);

        var motherBoardBuilder = new MotherBoardBuilder();
        motherBoardBuilder.AddSocket(new Socket("AM4")).AddPciAmount(4).AddSataAmount(4)
            .AddChipset(new Chipset(new List<int> { 4400, 3300 }, true)).AddDdr(new Ddr4()).AddRamAmount(4)
            .AddFormFactor(new MotherBoardFormFactor("ATX"))
            .AddBios(repository.GetById(2) as IBios);

        IMotherBoard motherBoard = motherBoardBuilder.Build();
        repository.Add(3, motherBoard);

        var coolerBuilder = new CoolerBuilder();
        coolerBuilder.AddTdp(150).AddSize(15).AddSockets(new List<Socket> { new Socket("AM4") });

        ICoolingSystem coolingSystem = coolerBuilder.Build();
        repository.Add(4, coolingSystem);

        var ramBuilder = new RamBuilder();
        ramBuilder.AddCapacity(4).AddAmount(4).AddJedecAndVoltage(new List<JedecVoltagePair>
                { new JedecVoltagePair(4400, 100), new JedecVoltagePair(3300, 150) })
            .AddXmpProfiles(new List<Xmp> { new Xmp(new Timing(1, 2, 3, 4), 150, 4500) })
            .AddFormFactor(new RamFormFactor("DIMM")).AddDdrStandard(new Ddr4()).AddPowerConsumption(100);

        IRam ram = ramBuilder.Build();
        repository.Add(5, ram);

        var gpuBuilder = new GpuBuilder();
        gpuBuilder.AddHeight(10).AddWidth(20).AddVideoMemory(8).AddPciVersion(9).AddFrequency(3000)
            .AddPowerConsumption(100);

        IGpu gpu = gpuBuilder.Build();
        repository.Add(6, gpu);

        var ssdBuilder = new SsdBuilder();
        ssdBuilder.AddConnection(new ConnectorType.PciConnector()).AddCapacity(250).AddMaxSpeed(1000)
            .AddPowerConsumption(100);

        ISsd ssd = ssdBuilder.Build();
        repository.Add(7, ssd);

        var hddBuilder = new HddBuilder();
        hddBuilder.AddCapacity(250).AddRotatingSpeed(1000).AddPowerConsumption(100);

        IHdd hdd = hddBuilder.Build();
        repository.Add(8, hdd);

        var hullBuilder = new HullBuilder();
        hullBuilder.AddMaxHeight(20).AddMaxWidth(30).AddHeight(25).AddWidth(35).AddLength(40)
            .AddSupportedFormFactorsMotherboard(new List<MotherBoardFormFactor> { new MotherBoardFormFactor("ATX") });

        IHull hull = hullBuilder.Build();
        repository.Add(9, hull);

        var powerUnitBuilder = new PowerUnitBuilder();
        powerUnitBuilder.AddMaxPowerConsumption(800);

        IPowerUnit powerUnit = powerUnitBuilder.Build();
        repository.Add(10, powerUnit);

        var wifiAdapterBuilder = new WifiAdapterBuilder();
        wifiAdapterBuilder.AddPowerConsumption(100).AddPciVersion(1).AddBluetoothModule(true)
            .AddWifiStandardVersion(new WifiStandardVersion("802.11a"));

        IWifiAdapter wifiAdapter = wifiAdapterBuilder.Build();
        repository.Add(11, wifiAdapter);

        var computerBuilder = new ComputerBuilder();
        computerBuilder.AddMotherBoard(repository.GetById(3) as IMotherBoard).AddCpu(repository.GetById(1) as ICpu)
            .AddCooler(repository.GetById(4) as ICoolingSystem).AddRam(repository.GetById(5) as IRam)
            .AddMemory(repository.GetById(7) as IMemory).AddMemory(repository.GetById(8) as IMemory)
            .AddHull(repository.GetById(9) as IHull).AddPowerUnit(repository.GetById(10) as IPowerUnit)
            .AddGpu(repository.GetById(6) as IGpu).AddWiFi(repository.GetById(11) as IWifiAdapter);

        // Act
        ValidateResultComputer result = computerBuilder.Build();

        // Assert
        Assert.True(result is ValidateResultComputer.SuccessValidateResultComputer);
    }

    [Theory]
    [InlineData("Power consumption exceeded the recommended interval")]
    public void ComputerWithWarningConsumption(string expectedMessage)
    {
        // Arrange
        var repository = new Repository();

        var cpuBuilder = new CpuBuilder();
        cpuBuilder.AddCoreFrequency(4400).AddSocket(new Socket("AM4")).AddTdp(100).AddCoreAmount(4)
            .AddMemoryFrequencies(new List<int> { 4400, 3300 }).AddVideoCore(true).AddPowerConsumption(100);
        ICpu cpu = cpuBuilder.Build();
        repository.Add(1, cpu);

        var biosBuilder = new BiosBuilder();
        biosBuilder.AddType("BIOS").AddVersion("1.1").AddCpuList(new List<ICpu>
        {
            repository.GetById(1) as ICpu ?? throw new InvalidDataException(),
        });

        IBios bios = biosBuilder.Build();
        repository.Add(2, bios);

        var motherBoardBuilder = new MotherBoardBuilder();
        motherBoardBuilder.AddSocket(new Socket("AM4")).AddPciAmount(4).AddSataAmount(4)
            .AddChipset(new Chipset(new List<int> { 4400, 3300 }, true)).AddDdr(new Ddr4()).AddRamAmount(4)
            .AddFormFactor(new MotherBoardFormFactor("ATX"))
            .AddBios(repository.GetById(2) as IBios);

        IMotherBoard motherBoard = motherBoardBuilder.Build();
        repository.Add(3, motherBoard);

        var coolerBuilder = new CoolerBuilder();
        coolerBuilder.AddTdp(150).AddSize(15).AddSockets(new List<Socket> { new Socket("AM4") });

        ICoolingSystem coolingSystem = coolerBuilder.Build();
        repository.Add(4, coolingSystem);

        var ramBuilder = new RamBuilder();
        ramBuilder.AddCapacity(4).AddAmount(4).AddJedecAndVoltage(new List<JedecVoltagePair>
                { new JedecVoltagePair(4400, 100), new JedecVoltagePair(3300, 150) })
            .AddXmpProfiles(new List<Xmp> { new Xmp(new Timing(1, 2, 3, 4), 150, 4500) })
            .AddFormFactor(new RamFormFactor("DIMM")).AddDdrStandard(new Ddr4()).AddPowerConsumption(100);

        IRam ram = ramBuilder.Build();
        repository.Add(5, ram);

        var gpuBuilder = new GpuBuilder();
        gpuBuilder.AddHeight(10).AddWidth(20).AddVideoMemory(8).AddPciVersion(9).AddFrequency(3000)
            .AddPowerConsumption(300);

        IGpu gpu = gpuBuilder.Build();
        repository.Add(6, gpu);

        var ssdBuilder = new SsdBuilder();
        ssdBuilder.AddConnection(new ConnectorType.PciConnector()).AddCapacity(250).AddMaxSpeed(1000)
            .AddPowerConsumption(100);

        ISsd ssd = ssdBuilder.Build();
        repository.Add(7, ssd);

        var hddBuilder = new HddBuilder();
        hddBuilder.AddCapacity(250).AddRotatingSpeed(1000).AddPowerConsumption(100);

        IHdd hdd = hddBuilder.Build();
        repository.Add(8, hdd);

        var hullBuilder = new HullBuilder();
        hullBuilder.AddMaxHeight(20).AddMaxWidth(30).AddHeight(25).AddWidth(35).AddLength(40)
            .AddSupportedFormFactorsMotherboard(new List<MotherBoardFormFactor> { new MotherBoardFormFactor("ATX") });

        IHull hull = hullBuilder.Build();
        repository.Add(9, hull);

        var powerUnitBuilder = new PowerUnitBuilder();
        powerUnitBuilder.AddMaxPowerConsumption(800);

        IPowerUnit powerUnit = powerUnitBuilder.Build();
        repository.Add(10, powerUnit);

        var wifiAdapterBuilder = new WifiAdapterBuilder();
        wifiAdapterBuilder.AddPowerConsumption(100).AddPciVersion(1).AddBluetoothModule(true)
            .AddWifiStandardVersion(new WifiStandardVersion("802.11a"));

        IWifiAdapter wifiAdapter = wifiAdapterBuilder.Build();
        repository.Add(11, wifiAdapter);

        var computerBuilder = new ComputerBuilder();
        computerBuilder.AddMotherBoard(repository.GetById(3) as IMotherBoard).AddCpu(repository.GetById(1) as ICpu)
            .AddCooler(repository.GetById(4) as ICoolingSystem).AddRam(repository.GetById(5) as IRam)
            .AddMemory(repository.GetById(7) as IMemory).AddMemory(repository.GetById(8) as IMemory)
            .AddHull(repository.GetById(9) as IHull).AddPowerUnit(repository.GetById(10) as IPowerUnit)
            .AddGpu(repository.GetById(6) as IGpu).AddWiFi(repository.GetById(11) as IWifiAdapter);

        // Act
        ValidateResultComputer result = computerBuilder.Build();

        // Assert
        Assert.True(result is ValidateResultComputer.SuccessWithWarningValidateResultComputer);
        if (result is ValidateResultComputer.SuccessWithWarningValidateResultComputer resultWithWarning)
        {
            if (resultWithWarning.Message.ToList()[0] is ValidateResultComponent.WarningValidateResultComponent
                warningMessage)
            {
                Assert.Equal(expectedMessage, warningMessage.Message);
            }
        }
    }

    [Theory]
    [InlineData("CPU's TDP is bigger than max TDP of cooler")]
    public void ComputerWithWarningTdp(string expectedMessage)
    {
        // Arrange
        var repository = new Repository();

        var cpuBuilder = new CpuBuilder();
        cpuBuilder.AddCoreFrequency(4400).AddSocket(new Socket("AM4")).AddTdp(200).AddCoreAmount(4)
            .AddMemoryFrequencies(new List<int> { 4400, 3300 }).AddVideoCore(true).AddPowerConsumption(100);
        ICpu cpu = cpuBuilder.Build();
        repository.Add(1, cpu);

        var biosBuilder = new BiosBuilder();
        biosBuilder.AddType("BIOS").AddVersion("1.1").AddCpuList(new List<ICpu>
        {
            repository.GetById(1) as ICpu ?? throw new InvalidDataException(),
        });

        IBios bios = biosBuilder.Build();
        repository.Add(2, bios);

        var motherBoardBuilder = new MotherBoardBuilder();
        motherBoardBuilder.AddSocket(new Socket("AM4")).AddPciAmount(4).AddSataAmount(4)
            .AddChipset(new Chipset(new List<int> { 4400, 3300 }, true)).AddDdr(new Ddr4()).AddRamAmount(4)
            .AddFormFactor(new MotherBoardFormFactor("ATX"))
            .AddBios(repository.GetById(2) as IBios);

        IMotherBoard motherBoard = motherBoardBuilder.Build();
        repository.Add(3, motherBoard);

        var coolerBuilder = new CoolerBuilder();
        coolerBuilder.AddTdp(150).AddSize(15).AddSockets(new List<Socket> { new Socket("AM4") });

        ICoolingSystem coolingSystem = coolerBuilder.Build();
        repository.Add(4, coolingSystem);

        var ramBuilder = new RamBuilder();
        ramBuilder.AddCapacity(4).AddAmount(4).AddJedecAndVoltage(new List<JedecVoltagePair>
                { new JedecVoltagePair(4400, 100), new JedecVoltagePair(3300, 150) })
            .AddXmpProfiles(new List<Xmp> { new Xmp(new Timing(1, 2, 3, 4), 150, 4500) })
            .AddFormFactor(new RamFormFactor("DIMM")).AddDdrStandard(new Ddr4()).AddPowerConsumption(100);

        IRam ram = ramBuilder.Build();
        repository.Add(5, ram);

        var gpuBuilder = new GpuBuilder();
        gpuBuilder.AddHeight(10).AddWidth(20).AddVideoMemory(8).AddPciVersion(9).AddFrequency(3000)
            .AddPowerConsumption(100);

        IGpu gpu = gpuBuilder.Build();
        repository.Add(6, gpu);

        var ssdBuilder = new SsdBuilder();
        ssdBuilder.AddConnection(new ConnectorType.PciConnector()).AddCapacity(250).AddMaxSpeed(1000)
            .AddPowerConsumption(100);

        ISsd ssd = ssdBuilder.Build();
        repository.Add(7, ssd);

        var hddBuilder = new HddBuilder();
        hddBuilder.AddCapacity(250).AddRotatingSpeed(1000).AddPowerConsumption(100);

        IHdd hdd = hddBuilder.Build();
        repository.Add(8, hdd);

        var hullBuilder = new HullBuilder();
        hullBuilder.AddMaxHeight(20).AddMaxWidth(30).AddHeight(25).AddWidth(35).AddLength(40)
            .AddSupportedFormFactorsMotherboard(new List<MotherBoardFormFactor> { new MotherBoardFormFactor("ATX") });

        IHull hull = hullBuilder.Build();
        repository.Add(9, hull);

        var powerUnitBuilder = new PowerUnitBuilder();
        powerUnitBuilder.AddMaxPowerConsumption(800);

        IPowerUnit powerUnit = powerUnitBuilder.Build();
        repository.Add(10, powerUnit);

        var wifiAdapterBuilder = new WifiAdapterBuilder();
        wifiAdapterBuilder.AddPowerConsumption(100).AddPciVersion(1).AddBluetoothModule(true)
            .AddWifiStandardVersion(new WifiStandardVersion("802.11a"));

        IWifiAdapter wifiAdapter = wifiAdapterBuilder.Build();
        repository.Add(11, wifiAdapter);

        var computerBuilder = new ComputerBuilder();
        computerBuilder.AddMotherBoard(repository.GetById(3) as IMotherBoard).AddCpu(repository.GetById(1) as ICpu)
            .AddCooler(repository.GetById(4) as ICoolingSystem).AddRam(repository.GetById(5) as IRam)
            .AddMemory(repository.GetById(7) as IMemory).AddMemory(repository.GetById(8) as IMemory)
            .AddHull(repository.GetById(9) as IHull).AddPowerUnit(repository.GetById(10) as IPowerUnit)
            .AddGpu(repository.GetById(6) as IGpu).AddWiFi(repository.GetById(11) as IWifiAdapter);

        // Act
        ValidateResultComputer result = computerBuilder.Build();

        // Assert
        Assert.True(result is ValidateResultComputer.SuccessWithWarningValidateResultComputer);
        if (result is ValidateResultComputer.SuccessWithWarningValidateResultComputer resultWithWarning)
        {
            if (resultWithWarning.Message.ToList()[0] is ValidateResultComponent.WarningValidateResultComponent
                warningMessage)
            {
                Assert.Equal(expectedMessage, warningMessage.Message);
            }
        }
    }

    [Theory]
    [InlineData("There is no video core and gpu")]
    public void ComputerWithFailedCpuAndGpu(string expectedMessage)
    {
        // Arrange
        var repository = new Repository();

        var cpuBuilder = new CpuBuilder();
        cpuBuilder.AddCoreFrequency(4400).AddSocket(new Socket("AM4")).AddTdp(100).AddCoreAmount(4)
            .AddMemoryFrequencies(new List<int> { 4400, 3300 }).AddVideoCore(false).AddPowerConsumption(100);
        ICpu cpu = cpuBuilder.Build();
        repository.Add(1, cpu);

        var biosBuilder = new BiosBuilder();
        biosBuilder.AddType("BIOS").AddVersion("1.1").AddCpuList(new List<ICpu>
        {
            repository.GetById(1) as ICpu ?? throw new InvalidDataException(),
        });

        IBios bios = biosBuilder.Build();
        repository.Add(2, bios);

        var motherBoardBuilder = new MotherBoardBuilder();
        motherBoardBuilder.AddSocket(new Socket("AM4")).AddPciAmount(4).AddSataAmount(4)
            .AddChipset(new Chipset(new List<int> { 4400, 3300 }, true)).AddDdr(new Ddr4()).AddRamAmount(4)
            .AddFormFactor(new MotherBoardFormFactor("ATX"))
            .AddBios(repository.GetById(2) as IBios);

        IMotherBoard motherBoard = motherBoardBuilder.Build();
        repository.Add(3, motherBoard);

        var coolerBuilder = new CoolerBuilder();
        coolerBuilder.AddTdp(150).AddSize(15).AddSockets(new List<Socket> { new Socket("AM4") });

        ICoolingSystem coolingSystem = coolerBuilder.Build();
        repository.Add(4, coolingSystem);

        var ramBuilder = new RamBuilder();
        ramBuilder.AddCapacity(4).AddAmount(4).AddJedecAndVoltage(new List<JedecVoltagePair>
                { new JedecVoltagePair(4400, 100), new JedecVoltagePair(3300, 150) })
            .AddXmpProfiles(new List<Xmp> { new Xmp(new Timing(1, 2, 3, 4), 150, 4500) })
            .AddFormFactor(new RamFormFactor("DIMM")).AddDdrStandard(new Ddr4()).AddPowerConsumption(100);

        IRam ram = ramBuilder.Build();
        repository.Add(5, ram);

        var ssdBuilder = new SsdBuilder();
        ssdBuilder.AddConnection(new ConnectorType.PciConnector()).AddCapacity(250).AddMaxSpeed(1000)
            .AddPowerConsumption(100);

        ISsd ssd = ssdBuilder.Build();
        repository.Add(7, ssd);

        var hddBuilder = new HddBuilder();
        hddBuilder.AddCapacity(250).AddRotatingSpeed(1000).AddPowerConsumption(100);

        IHdd hdd = hddBuilder.Build();
        repository.Add(8, hdd);

        var hullBuilder = new HullBuilder();
        hullBuilder.AddMaxHeight(20).AddMaxWidth(30).AddHeight(25).AddWidth(35).AddLength(40)
            .AddSupportedFormFactorsMotherboard(new List<MotherBoardFormFactor> { new MotherBoardFormFactor("ATX") });

        IHull hull = hullBuilder.Build();
        repository.Add(9, hull);

        var powerUnitBuilder = new PowerUnitBuilder();
        powerUnitBuilder.AddMaxPowerConsumption(800);

        IPowerUnit powerUnit = powerUnitBuilder.Build();
        repository.Add(10, powerUnit);

        var wifiAdapterBuilder = new WifiAdapterBuilder();
        wifiAdapterBuilder.AddPowerConsumption(100).AddPciVersion(1).AddBluetoothModule(true)
            .AddWifiStandardVersion(new WifiStandardVersion("802.11a"));

        IWifiAdapter wifiAdapter = wifiAdapterBuilder.Build();
        repository.Add(11, wifiAdapter);

        var computerBuilder = new ComputerBuilder();
        computerBuilder.AddMotherBoard(repository.GetById(3) as IMotherBoard).AddCpu(repository.GetById(1) as ICpu)
            .AddCooler(repository.GetById(4) as ICoolingSystem).AddRam(repository.GetById(5) as IRam)
            .AddMemory(repository.GetById(7) as IMemory).AddMemory(repository.GetById(8) as IMemory)
            .AddHull(repository.GetById(9) as IHull).AddPowerUnit(repository.GetById(10) as IPowerUnit)
            .AddWiFi(repository.GetById(11) as IWifiAdapter);

        // Act
        ValidateResultComputer result = computerBuilder.Build();

        // Assert
        Assert.True(result is ValidateResultComputer.FailedValidateResultComputer);
        if (result is ValidateResultComputer.FailedValidateResultComputer resultFailed)
        {
            if (resultFailed.Message.ToList()[0] is ValidateResultComponent.FailedValidateResultComponent
                failedMessage)
            {
                Assert.Equal(expectedMessage, failedMessage.Message);
            }
        }
    }

    [Theory]
    [InlineData("Motherboard doesn't match hull")]
    public void ComputerWithFailedMotherBoardFormFactor(string expectedMessage)
    {
        // Arrange
        var repository = new Repository();

        var cpuBuilder = new CpuBuilder();
        cpuBuilder.AddCoreFrequency(4400).AddSocket(new Socket("AM4")).AddTdp(100).AddCoreAmount(4)
            .AddMemoryFrequencies(new List<int> { 4400, 3300 }).AddVideoCore(false).AddPowerConsumption(100);
        ICpu cpu = cpuBuilder.Build();
        repository.Add(1, cpu);

        var biosBuilder = new BiosBuilder();
        biosBuilder.AddType("BIOS").AddVersion("1.1").AddCpuList(new List<ICpu>
        {
            repository.GetById(1) as ICpu ?? throw new InvalidDataException(),
        });

        IBios bios = biosBuilder.Build();
        repository.Add(2, bios);

        var motherBoardBuilder = new MotherBoardBuilder();
        motherBoardBuilder.AddSocket(new Socket("AM4")).AddPciAmount(4).AddSataAmount(4)
            .AddChipset(new Chipset(new List<int> { 4400, 3300 }, true)).AddDdr(new Ddr4()).AddRamAmount(4)
            .AddFormFactor(new MotherBoardFormFactor("ATX"))
            .AddBios(repository.GetById(2) as IBios);

        IMotherBoard motherBoard = motherBoardBuilder.Build();
        repository.Add(3, motherBoard);

        var coolerBuilder = new CoolerBuilder();
        coolerBuilder.AddTdp(150).AddSize(15).AddSockets(new List<Socket> { new Socket("AM4") });

        ICoolingSystem coolingSystem = coolerBuilder.Build();
        repository.Add(4, coolingSystem);

        var ramBuilder = new RamBuilder();
        ramBuilder.AddCapacity(4).AddAmount(4).AddJedecAndVoltage(new List<JedecVoltagePair>
                { new JedecVoltagePair(4400, 100), new JedecVoltagePair(3300, 150) })
            .AddXmpProfiles(new List<Xmp> { new Xmp(new Timing(1, 2, 3, 4), 150, 4500) })
            .AddFormFactor(new RamFormFactor("DIMM")).AddDdrStandard(new Ddr4()).AddPowerConsumption(100);

        IRam ram = ramBuilder.Build();
        repository.Add(5, ram);

        var gpuBuilder = new GpuBuilder();
        gpuBuilder.AddHeight(10).AddWidth(20).AddVideoMemory(8).AddPciVersion(9).AddFrequency(3000)
            .AddPowerConsumption(100);

        IGpu gpu = gpuBuilder.Build();
        repository.Add(6, gpu);

        var ssdBuilder = new SsdBuilder();
        ssdBuilder.AddConnection(new ConnectorType.PciConnector()).AddCapacity(250).AddMaxSpeed(1000)
            .AddPowerConsumption(100);

        ISsd ssd = ssdBuilder.Build();
        repository.Add(7, ssd);

        var hddBuilder = new HddBuilder();
        hddBuilder.AddCapacity(250).AddRotatingSpeed(1000).AddPowerConsumption(100);

        IHdd hdd = hddBuilder.Build();
        repository.Add(8, hdd);

        var hullBuilder = new HullBuilder();
        hullBuilder.AddMaxHeight(20).AddMaxWidth(30).AddHeight(25).AddWidth(35).AddLength(40)
            .AddSupportedFormFactorsMotherboard(
                new List<MotherBoardFormFactor> { new MotherBoardFormFactor("miniATX") });

        IHull hull = hullBuilder.Build();
        repository.Add(9, hull);

        var powerUnitBuilder = new PowerUnitBuilder();
        powerUnitBuilder.AddMaxPowerConsumption(800);

        IPowerUnit powerUnit = powerUnitBuilder.Build();
        repository.Add(10, powerUnit);

        var wifiAdapterBuilder = new WifiAdapterBuilder();
        wifiAdapterBuilder.AddPowerConsumption(100).AddPciVersion(1).AddBluetoothModule(true)
            .AddWifiStandardVersion(new WifiStandardVersion("802.11a"));

        IWifiAdapter wifiAdapter = wifiAdapterBuilder.Build();
        repository.Add(11, wifiAdapter);

        var computerBuilder = new ComputerBuilder();
        computerBuilder.AddMotherBoard(repository.GetById(3) as IMotherBoard).AddCpu(repository.GetById(1) as ICpu)
            .AddCooler(repository.GetById(4) as ICoolingSystem).AddRam(repository.GetById(5) as IRam)
            .AddMemory(repository.GetById(7) as IMemory).AddMemory(repository.GetById(8) as IMemory)
            .AddHull(repository.GetById(9) as IHull).AddPowerUnit(repository.GetById(10) as IPowerUnit)
            .AddGpu(repository.GetById(6) as IGpu).AddWiFi(repository.GetById(11) as IWifiAdapter);

        // Act
        ValidateResultComputer result = computerBuilder.Build();

        // Assert
        Assert.True(result is ValidateResultComputer.FailedValidateResultComputer);
        if (result is ValidateResultComputer.FailedValidateResultComputer resultFailed)
        {
            if (resultFailed.Message.ToList()[0] is ValidateResultComponent.FailedValidateResultComponent
                failedMessage)
            {
                Assert.Equal(expectedMessage, failedMessage.Message);
            }
        }
    }

    [Theory]
    [InlineData("Ram's DDR standard doesn't match motherboard's standard")]
    public void ComputerWithFailedRamDdrStandard(string expectedMessage)
    {
        // Arrange
        var repository = new Repository();

        var cpuBuilder = new CpuBuilder();
        cpuBuilder.AddCoreFrequency(4400).AddSocket(new Socket("AM4")).AddTdp(100).AddCoreAmount(4)
            .AddMemoryFrequencies(new List<int> { 4400, 3300 }).AddVideoCore(false).AddPowerConsumption(100);
        ICpu cpu = cpuBuilder.Build();
        repository.Add(1, cpu);

        var biosBuilder = new BiosBuilder();
        biosBuilder.AddType("BIOS").AddVersion("1.1").AddCpuList(new List<ICpu>
        {
            repository.GetById(1) as ICpu ?? throw new InvalidDataException(),
        });

        IBios bios = biosBuilder.Build();
        repository.Add(2, bios);

        var motherBoardBuilder = new MotherBoardBuilder();
        motherBoardBuilder.AddSocket(new Socket("AM4")).AddPciAmount(4).AddSataAmount(4)
            .AddChipset(new Chipset(new List<int> { 4400, 3300 }, true)).AddDdr(new Ddr4()).AddRamAmount(4)
            .AddFormFactor(new MotherBoardFormFactor("ATX"))
            .AddBios(repository.GetById(2) as IBios);

        IMotherBoard motherBoard = motherBoardBuilder.Build();
        repository.Add(3, motherBoard);

        var coolerBuilder = new CoolerBuilder();
        coolerBuilder.AddTdp(150).AddSize(15).AddSockets(new List<Socket> { new Socket("AM4") });

        ICoolingSystem coolingSystem = coolerBuilder.Build();
        repository.Add(4, coolingSystem);

        var ramBuilder = new RamBuilder();
        ramBuilder.AddCapacity(4).AddAmount(4).AddJedecAndVoltage(new List<JedecVoltagePair>
                { new JedecVoltagePair(4400, 100), new JedecVoltagePair(3300, 150) })
            .AddXmpProfiles(new List<Xmp> { new Xmp(new Timing(1, 2, 3, 4), 150, 4500) })
            .AddFormFactor(new RamFormFactor("DIMM")).AddDdrStandard(new Ddr5()).AddPowerConsumption(100);

        IRam ram = ramBuilder.Build();
        repository.Add(5, ram);

        var gpuBuilder = new GpuBuilder();
        gpuBuilder.AddHeight(10).AddWidth(20).AddVideoMemory(8).AddPciVersion(9).AddFrequency(3000)
            .AddPowerConsumption(100);

        IGpu gpu = gpuBuilder.Build();
        repository.Add(6, gpu);

        var ssdBuilder = new SsdBuilder();
        ssdBuilder.AddConnection(new ConnectorType.PciConnector()).AddCapacity(250).AddMaxSpeed(1000)
            .AddPowerConsumption(100);

        ISsd ssd = ssdBuilder.Build();
        repository.Add(7, ssd);

        var hddBuilder = new HddBuilder();
        hddBuilder.AddCapacity(250).AddRotatingSpeed(1000).AddPowerConsumption(100);

        IHdd hdd = hddBuilder.Build();
        repository.Add(8, hdd);

        var hullBuilder = new HullBuilder();
        hullBuilder.AddMaxHeight(20).AddMaxWidth(30).AddHeight(25).AddWidth(35).AddLength(40)
            .AddSupportedFormFactorsMotherboard(
                new List<MotherBoardFormFactor> { new MotherBoardFormFactor("ATX") });

        IHull hull = hullBuilder.Build();
        repository.Add(9, hull);

        var powerUnitBuilder = new PowerUnitBuilder();
        powerUnitBuilder.AddMaxPowerConsumption(800);

        IPowerUnit powerUnit = powerUnitBuilder.Build();
        repository.Add(10, powerUnit);

        var wifiAdapterBuilder = new WifiAdapterBuilder();
        wifiAdapterBuilder.AddPowerConsumption(100).AddPciVersion(1).AddBluetoothModule(true)
            .AddWifiStandardVersion(new WifiStandardVersion("802.11a"));

        IWifiAdapter wifiAdapter = wifiAdapterBuilder.Build();
        repository.Add(11, wifiAdapter);

        var computerBuilder = new ComputerBuilder();
        computerBuilder.AddMotherBoard(repository.GetById(3) as IMotherBoard).AddCpu(repository.GetById(1) as ICpu)
            .AddCooler(repository.GetById(4) as ICoolingSystem).AddRam(repository.GetById(5) as IRam)
            .AddMemory(repository.GetById(7) as IMemory).AddMemory(repository.GetById(8) as IMemory)
            .AddHull(repository.GetById(9) as IHull).AddPowerUnit(repository.GetById(10) as IPowerUnit)
            .AddGpu(repository.GetById(6) as IGpu).AddWiFi(repository.GetById(11) as IWifiAdapter);

        // Act
        ValidateResultComputer result = computerBuilder.Build();

        // Assert
        Assert.True(result is ValidateResultComputer.FailedValidateResultComputer);
        if (result is ValidateResultComputer.FailedValidateResultComputer resultFailed)
        {
            if (resultFailed.Message.ToList()[0] is ValidateResultComponent.FailedValidateResultComponent
                failedMessage)
            {
                Assert.Equal(expectedMessage, failedMessage.Message);
            }
        }
    }

    [Theory]
    [InlineData("There are no common suitable frequencies for the motherboard, RAM and CPU")]
    public void ComputerWithFailedFrequency(string expectedMessage)
    {
        // Arrange
        var repository = new Repository();

        var cpuBuilder = new CpuBuilder();
        cpuBuilder.AddCoreFrequency(4400).AddSocket(new Socket("AM4")).AddTdp(100).AddCoreAmount(4)
            .AddMemoryFrequencies(new List<int> { 4400 }).AddVideoCore(false).AddPowerConsumption(100);
        ICpu cpu = cpuBuilder.Build();
        repository.Add(1, cpu);

        var biosBuilder = new BiosBuilder();
        biosBuilder.AddType("BIOS").AddVersion("1.1").AddCpuList(new List<ICpu>
        {
            repository.GetById(1) as ICpu ?? throw new InvalidDataException(),
        });

        IBios bios = biosBuilder.Build();
        repository.Add(2, bios);

        var motherBoardBuilder = new MotherBoardBuilder();
        motherBoardBuilder.AddSocket(new Socket("AM4")).AddPciAmount(4).AddSataAmount(4)
            .AddChipset(new Chipset(new List<int> { 3300 }, true)).AddDdr(new Ddr4()).AddRamAmount(4)
            .AddFormFactor(new MotherBoardFormFactor("ATX"))
            .AddBios(repository.GetById(2) as IBios);

        IMotherBoard motherBoard = motherBoardBuilder.Build();
        repository.Add(3, motherBoard);

        var coolerBuilder = new CoolerBuilder();
        coolerBuilder.AddTdp(150).AddSize(15).AddSockets(new List<Socket> { new Socket("AM4") });

        ICoolingSystem coolingSystem = coolerBuilder.Build();
        repository.Add(4, coolingSystem);

        var ramBuilder = new RamBuilder();
        ramBuilder.AddCapacity(4).AddAmount(4).AddJedecAndVoltage(new List<JedecVoltagePair>
                { new JedecVoltagePair(5400, 100), new JedecVoltagePair(2300, 150) })
            .AddXmpProfiles(new List<Xmp> { new Xmp(new Timing(1, 2, 3, 4), 150, 4500) })
            .AddFormFactor(new RamFormFactor("DIMM")).AddDdrStandard(new Ddr4()).AddPowerConsumption(100);

        IRam ram = ramBuilder.Build();
        repository.Add(5, ram);

        var gpuBuilder = new GpuBuilder();
        gpuBuilder.AddHeight(10).AddWidth(20).AddVideoMemory(8).AddPciVersion(9).AddFrequency(3000)
            .AddPowerConsumption(100);

        IGpu gpu = gpuBuilder.Build();
        repository.Add(6, gpu);

        var ssdBuilder = new SsdBuilder();
        ssdBuilder.AddConnection(new ConnectorType.PciConnector()).AddCapacity(250).AddMaxSpeed(1000)
            .AddPowerConsumption(100);

        ISsd ssd = ssdBuilder.Build();
        repository.Add(7, ssd);

        var hddBuilder = new HddBuilder();
        hddBuilder.AddCapacity(250).AddRotatingSpeed(1000).AddPowerConsumption(100);

        IHdd hdd = hddBuilder.Build();
        repository.Add(8, hdd);

        var hullBuilder = new HullBuilder();
        hullBuilder.AddMaxHeight(20).AddMaxWidth(30).AddHeight(25).AddWidth(35).AddLength(40)
            .AddSupportedFormFactorsMotherboard(
                new List<MotherBoardFormFactor> { new MotherBoardFormFactor("ATX") });

        IHull hull = hullBuilder.Build();
        repository.Add(9, hull);

        var powerUnitBuilder = new PowerUnitBuilder();
        powerUnitBuilder.AddMaxPowerConsumption(800);

        IPowerUnit powerUnit = powerUnitBuilder.Build();
        repository.Add(10, powerUnit);

        var wifiAdapterBuilder = new WifiAdapterBuilder();
        wifiAdapterBuilder.AddPowerConsumption(100).AddPciVersion(1).AddBluetoothModule(true)
            .AddWifiStandardVersion(new WifiStandardVersion("802.11a"));

        IWifiAdapter wifiAdapter = wifiAdapterBuilder.Build();
        repository.Add(11, wifiAdapter);

        var computerBuilder = new ComputerBuilder();
        computerBuilder.AddMotherBoard(repository.GetById(3) as IMotherBoard).AddCpu(repository.GetById(1) as ICpu)
            .AddCooler(repository.GetById(4) as ICoolingSystem).AddRam(repository.GetById(5) as IRam)
            .AddMemory(repository.GetById(7) as IMemory).AddMemory(repository.GetById(8) as IMemory)
            .AddHull(repository.GetById(9) as IHull).AddPowerUnit(repository.GetById(10) as IPowerUnit)
            .AddGpu(repository.GetById(6) as IGpu).AddWiFi(repository.GetById(11) as IWifiAdapter);

        // Act
        ValidateResultComputer result = computerBuilder.Build();

        // Assert
        Assert.True(result is ValidateResultComputer.FailedValidateResultComputer);
        if (result is ValidateResultComputer.FailedValidateResultComputer resultFailed)
        {
            if (resultFailed.Message.ToList()[0] is ValidateResultComponent.FailedValidateResultComponent
                failedMessage)
            {
                Assert.Equal(expectedMessage, failedMessage.Message);
            }
        }
    }
}
